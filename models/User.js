//[SECTION] Dependencies and Modules
  const mongoose = require('mongoose');
//[SECTION] Blueprint Schema
  const userSchema = new mongoose.Schema({
    
        firstName: {
            type: String,
            required: [true, 'First Name is Required']
    	},
        lastName: {
            type: String,
            required: [true, 'Last Name is Required']
    	},
        email: {
            type: String,
            required: [true, 'Email is Required']
        },
        password: {
            type: String,
            required: [true, 'Password is Required']
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        mobileNo: {
          type: String,
          required: [true, 'Mobile Number is Required']

        },
        cart: [{
            cartId: {type: String, required: [true, 'CartId is Required'] },
            addedtoCartOn: { type: Date, default: new Date()}
        }],
        orders: [{
            orderId: {
              type : String,
              required: [true, 'orderId is Required']
            },
            purchasedOn: {
              type: Date,
              default: new Date()
            }
        }]
  });
  
//[SECTION] Model
  const User = mongoose.model("User", userSchema)
  module.exports = User;
