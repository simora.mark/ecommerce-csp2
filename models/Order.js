//[SECTION] Dependencies and Modules
  const mongoose = require('mongoose')
  const products = require('../models/Product')
  const {price} = products
  
//[SECTION] Blueprint Schema
    const orderSchema = new mongoose.Schema({
      userId: {
            type: String,
            required: [true, "User ID is required"]
      },
      purchasedOn: {
            type: Date,
            default: new Date()
      },
      product: [{
            productId: { type : String, required: [true, 'Product Id is Required'] },
            quantity:  { type : String, required: [true, 'item quantity  is Required'] },
            
        }],
      totalAmount: { type: Number, default: 0 },
      address: { type: String, required: [true, "Address is required"] }
      
  
  });
//[SECTION] Model
  const Order = mongoose.model("Order", orderSchema)
  module.exports = Order;


//   productId: {
//       type: String,
//       required: [true, "Product ID is required"]
// },
// quantity: {
//       type: String,
//       default: 1
// },
// total: {
//       type: Number,
//       default: 0
// },

// purchasedOn: {
//       type: Date,
//       default: new Date()
// },
// customer: [{
//       userId: {type : String, required: [true, 'User Id is Required']},
//       email:  {type : String, required: [true, 'email  is Required']}
//   }]