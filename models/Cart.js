//[SECTION] Dependencies and Modules
const mongoose = require('mongoose')
const products = require('../models/Product')
const {price} = products

//[SECTION] Blueprint Schema
  const cartSchema = new mongoose.Schema({
    userId: {
          type: String,
          required: [true, "User ID is required"]
    },
    email: {
          type: String, required: [true, "User email is required"]
    },
    purchasedOn: {
          type: Date,
          default: new Date()
    },
    product: [{
          productId: { type : String, required: [true, 'User Id is Required'] },
          price: {type: Number, required: [true, 'Product Price is Required']},
          quantity:  { type : Number, default: 1 },
          totalAmount: { type: Number, default: 0 }
      }]
    

});
//[SECTION] Model
const Cart = mongoose.model("Cart", cartSchema)
module.exports = Cart;