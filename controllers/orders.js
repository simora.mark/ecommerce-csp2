//[SECTION] Dependencies and Modules
const Order = require('../models/Order');
const Cart = require('../models/Cart');
const User = require('../models/User');
const Product = require('../models/Product');
const dotenv = require("dotenv"); 

//[SECTION] Environment Setup
dotenv.config();

//[SECTION]FUNCTIONALITY POST

//! single ORDER

module.exports.createOrder = async (req, res) => {
  console.log(req.user.id)

  let cartId = req.body._id
  let proId = req.params.id
  
  // let customerDet = {      
  //   userId: req.user.id,
  //   email: req.user.email
  // }
  let isCreatedOrder =  await Cart.findById(cartId).then(foundCart => {
      console.log(`this is a test ${foundCart}`)
      //access subdocument of cart - subdocument product {obj.subdoc[].subdocobj}a
    // let cartprodid = foundCart.product.filter(function (e) { return e.productId })
    // let cartprodquantity = foundCart.product.filter(function (e) { return e.quantity })
    // let carttotal = foundCart.product.filter(function (e) { return e.totalAmount })

    let userCart = new Order({
        userId: req.user.id,
        product:[{
          productId: foundCart.product[0].productId,
          quantity:  foundCart.product[0].quantity,
        }],
        totalAmount: foundCart.product[0].totalAmount,
        address: req.body
      
      })
      
      return userCart.save().then(foundProduct => true, globalThis.getCartId = foundCart).catch(err => err.message)
      
  }) 

  if(isCreatedOrder !== true){
    return res.send({message: isCreatedOrder})
  } 

  
  let isUserUpdated = await User.findByIdAndUpdate({_id: req.user.id}, {$pull: {cart: {}}} ).then(userUpdate =>{
        

        let customerOrder = {
            orderId: getCartId._id 
        }

        userUpdate.orders.push(customerOrder);
        return userUpdate.save().then(userUpdate => true).catch(err => err.message)

  });
  //after successfull order this will delete the cart of a single order
  let isallOrderSuccess = await Cart.findByIdAndDelete(cartId).then().catch(err => res.send(err))

  // if(isallOrderSuccess !== true) {
  //   return res.send({ message: isUserUpdated})
  //  }   
   
    if(isUserUpdated !== true) {
      return res.send({ message: isUserUpdated})
     }   
     
     if(isCreatedOrder && isUserUpdated) {
      return res.send({ message: "Succesfully placed an Order."})
    }

}
//! END of get User Cart and Checkou single


// // //!for many cart to order
// module.exports.createallOrder = async (req, res) => {
//     console.log(req.user.id)

//     let isCreatedOrder =  await Cart.find({ userId: req.user.id}).then(foundCart => {
//      let lengthh = foundCart.length
//       // console.log(`this is a test ${foundCart}`)
//       console.log(`this is a test ${lengthh}`)
//       //access subdocument of cart - subdocument product {obj.subdoc[].subdocobj}a
    
//     for(let i = 0 ; i < foundCart.length; i++) {
//     let userCart = new Order({
//         userId: req.user.id,
//         product:[{
//           productId: foundCart[i].product[0].productId,
//           quantity:   foundCart[i].product[0].quantity
//         }],
//         totalAmount: foundCart[i].product[0].totalAmount,
//         address: req.body
        
//       })
      
//        return userCart.save().then(foundProduct => true, globalThis.getCartId = foundCart).catch(err => err.message)
//     }
     
      
//     }) 

//     let isUserUpdated = await User.findByIdAndUpdate({_id: req.user.id}, {$pull: {cart: {}}} ).then(userUpdate =>{
//       let lengthh = getCartId.length
//       console.log(`this is a test weh ${lengthh}`)
      
//       for(let i = 0 ; i < lengthh.length; i++) {
//           let customerOrder = {
//             orderId: getCartId._id 
//         }

//          userUpdate.orders.push(customerOrder);
//          return userUpdate.save().then(userUpdate => true).catch(err => err.message)
//         }


//     });
// //after successfull order this will delete the cart of a single order
//     let isallOrderSuccess = await Cart.findByIdAndDelete(req.body._id).then().catch(err => res.send(err))

// // if(isallOrderSuccess !== true) {
// //   return res.send({ message: isUserUpdated})
// //  }   
 
//       if(isUserUpdated !== true) {
//         return res.send({ message: isUserUpdated})
//       }   
      
//       if(isCreatedOrder && isUserUpdated) {
//         return res.send({ message: "Succesfully placed an Order."})
//       }

//     }



// //! END of get User Cart and Checkou ALl

//[SECTION]FUNCTIONALITY GET
  module.exports.getUserOrder = (req, res) => {
    User.findById(req.user.id).then(result => res.send(result)).catch(err => res.send(err))
  }
  module.exports.getAllOrder = (req, res) => {
    Order.find({}).then(result => res.send(result)).catch(err => res.send(err))
  }

  module.exports.getOrder = (req, res) => {
    let orderid = req.body.orderid
    Order.findById({orderid}).then(result => res.send(result)).catch(err => res.send(err))
  }

  
//[SECTION]FUNCTIONALITY PUT


//[SECTION]FUNCTIONALITY DEL
