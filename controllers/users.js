//[SECTION] Dependencies and Modules
const User = require('../models/User');
const bcrypt = require ("bcrypt");
const dotenv = require ("dotenv");
const auth = require("../loginAuths");
const req = require('express/lib/request');

//[SECTION] Environment Setup
dotenv.config();
const salt = parseInt(process.env.Salt);

//[SECTION] Functionality [CREATE]

//[Section] Register
module.exports.createUser = (info) => {
    let Useremail = info.email;
    let Userpassword = info.password;
    const regexPhoneNumber = /^(09|\+639)\d{9}$/;
    let UserMobileno = info.mobileNo;

    let newUser = new User({
        firstName: info.firstName,
        lastName: info.lastName,
        email: info.email,
        password: bcrypt.hashSync(Userpassword, salt),
        mobileNo: UserMobileno
    });
      
    function testMobNum (nstr) {
    return regexPhoneNumber.test(nstr);
    }

    return User.findOne({email: Useremail}).then((user) => {

        if(user){
            return res.send({message: `User already exists`});
        }
        
        if(UserMobileno.match(regexPhoneNumber)){
          return  newUser.save().then((savedUser, error) => {
           
            if (error) {
                return res.send({message: 'Failed to Save New User'});
            } else {
                
                return savedUser; 
            }
        });

        } else {
            return res.send({message:'invalid phone num'});
        }
    }).catch(err => {message:err})
  };

//[Section] LogIn
module.exports.loginUser = (req, res) => {
      console.log(req.body)

      User.findOne({email: req.body.email}).then(foundUser => {
  
        if(foundUser === null){
          return res.send ({message:'User Not Found'});
  
        } else {
  
          const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
  
          console.log(isPasswordCorrect)
          
          if(isPasswordCorrect){
  
            return res.send({accessToken: auth.createAccessToken(foundUser)})
  
          } else {
  
            return res.send({message:'Incorrect Password'})
          }
  
        }
  
      }).catch(err => {message:err.message})
  };

//!Get User Details

module.exports.getUserDetails = (req, res) => {

    console.log(req.user)

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => {message:err.message})
};

//[SECTION] Functionality [RETRIEVE]
  module.exports.getAllUsers = () => {
      return User.find({}).then(outcomeNiFind => {
      return outcomeNiFind;
      }).catch(err => {message:err.message})
  };

  module.exports.getUser = (id) => {
      
      return User.findById(id).then(resultOfQuery => {
          return resultOfQuery;
      }).catch(err => {message:err.message})
  };

  module.exports.getAllActiveUsers = () => {
  return User.find({isActive: true}).then(resultOfTheQuery => {
      return resultOfTheQuery;
  }).catch(err => {message:err.message})
  }

 
//[SECTION] Functionality [UPDATE]

  module.exports.updateUsertoAdmin = (id, details) => {
          
      let userEmail = details.email;
      let userisAdmin = details.isAdmin;       

      let updateUser = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: userEmail,
        isAdmin: userisAdmin,
        mobileNo: req.body.mobileNo
      }
      return User.findByIdAndUpdate(id, updateUser).then((userUpdated, error) => {
        
        if (error) {
            return res.send({message:'Failed to update User'});
        } else if(userUpdated.isAdmin === true){
            return res.send({message:'User is already an Admin'})
        } else {
            return res.send({message:'Successfully Updated User!'});
        };
      }).catch(err => {message:err.message})
  }

  module.exports.updateAdmintoUser = (id, details) => {
          
    let userEmail = details.email;
    let userisAdmin = details.isAdmin;       

    let updateUser = {
      email: userEmail,
      isAdmin: userisAdmin,
    }
    return User.findByIdAndUpdate(id, updateUser).then((userUpdated, error) => {
      
      if (error) {
          return res.send({message:'Failed to update User'});
      } else if(userUpdated.isAdmin === false){
          return res.send({message:'User is already non admin'})
      } else {
          return res.send({message:'Successfully Updated User!'});
      };
    }).catch(err => {message:err.message})
}

//[SECTION] Functionality [DELETE]
  module.exports.deleteUser = (id) => {
  
    return User.findByIdAndRemove(id).then((removedUser, err) => {
      
      if (removedUser){
          return res.send({message:'User Succesfully Deleted'})
      } else {
          return res.send({message:'No User Was Removed'})
      }
    }).catch(err => {message:err.message})
  }


  //[section]
  //changed return values to (objects) - res.send({message:'message'}) for front end REACT