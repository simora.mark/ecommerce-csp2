//[SECTION] Dependencies and Modules
const Product = require('../models/Product');
const dotenv = require ("dotenv");
const res = require('express/lib/response');

//[SECTION] Environment Setup
dotenv.config();

//[SECTION] Functionality [CREATE]
module.exports.createProduct = (info) => {

let productName = info.name;
let productDescription = info.description;
let productPrice = info.price;

let newProduct = new Product({
    name: productName,
    description: productDescription,
    price: productPrice
});
   
return Product.findOne({name: productName}).then(product => {
    if(product){
        return res.send({message:'Product already exists'})
    } else {

       return newProduct.save().then((savedProduct, error) => {
       
            if (error) {
                 
                return res.send({message:'Failed to Save New Product'});
            } else {
                
                return savedProduct; 
            }
        });
    }

}).catch(err => {message:err.message})

};


//[SECTION] Functionality [RETRIEVE]

module.exports.getAllProducts = () => {
    return Product.find({}).then(outcomeNiFind => {
        return outcomeNiFind
    }).catch(err => {message:err.message})
}

module.exports.getProduct = (id) => {
    
    return Product.findById(id).then(resultOfQuery => {
        return resultOfQuery;
    }).catch(err => {message:err.message})
};

module.exports.getAllActiveProducts = () => {
    return Product.find({isActive: true}).then(resultOfTheQuery => {
    return resultOfTheQuery;
}).catch(err => {message:err.message})
}

module.exports.getAllArchivedProducts = () => {
    return Product.find({isActive: false}).then(resultOfTheQuery => {
    return resultOfTheQuery;
});
}

//[SECTION] Functionality [UPDATE]

module.exports.updateProduct = (id, details) => {
    let pName = details.name; 
    let pDesc = details.description;
    let pCost = details.price;   
    let updatedProduct = {
    name: pName,
    description: pDesc,
    price: pCost
    }
    return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
        if (err) {
        return res.send({message:'Failed to update Product'}); 
        } else {
        return res.send({message:'Successfully Updated Product'}); 
        }
    }).catch(err => {message:err.message})
}

module.exports.archive = (id) => {
    let updates = {
    isActive:false
    }
    return Product.findByIdAndUpdate(id, updates).then((archived, error) => {
    
        if(archived === null){
            return {message:'Failed to archive product'}; 
        }else if(archived.isActive === false){
            return {message:`The product , is already  deactivated`};
        }else{
            return {message:`The product has been deactivated`};
        }

    }).catch(err => {message:err.message})
};

module.exports.activate = (id) => {

    let updates = {
    isActive:true
    }

    return Product.findByIdAndUpdate(id, updates).then((reactivate, error) => {
    
        if (reactivate === null) {
            return {message:'Failed to activate product, no product found'}
        } else if(reactivate.isActive === true){
            return {message:`The product  is already activated`};
        } else {
        return {message:`The product , has been activated successfully`};
        };
        
    }).catch(err => {message:err.message})
};

//[SECTION] Functionality [DELETE]
module.exports.deleteProduct = (id) => {

    return Product.findByIdAndRemove(id).then((removedProduct, err) => {

    if (removedProduct === null){
        return {message:'No Product Was Removed, Product does not exist'}
    } else {
        return true
    }
    }).catch(err => {message:err.message})
}