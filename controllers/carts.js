//[SECTION] Dependencies and Modules
const Cart = require('../models/Cart');
const User = require('../models/User');
const Product = require('../models/Product');
const dotenv = require("dotenv"); 
const res = require('express/lib/response');

//[SECTION] Environment Setup
dotenv.config();

//[SECTION]FUNCTIONALITY POST

module.exports.userCart = async (req, res) => {
  console.log(req.user.id)
  
  let prodId = req.params.id
  
  let isCreatedCart =  await Product.findById(prodId).then((foundProduct, resCart) => {
      console.log( 'THis is Test1: foundProduct' + foundProduct)
      let userCart = new Cart({
          userId: req.user.id,
          email: req.user.email,
          product: [{
            productId: foundProduct.id,
            price:foundProduct.price, 
            quantity: req.body.quantity, 
            totalAmount: req.body.quantity * foundProduct.price
          }]
        })

      //start
    return Cart.findOne({userId: userCart.userId, "product.productId": userCart.product[0].productId}).then(res => {
              if(res){
                let newquantity = res.product[0].quantity + userCart.product[0].quantity
                let newtotal = res.product[0].totalAmount + userCart.product[0].totalAmount
                userCart.product[0].quantity = newquantity
                userCart.product[0].totalAmount = newtotal
                  let settingUpdate = {
                      userId: req.user.id,
                      email: req.user.email,
                      purchasedOn:new Date(),
                      product: [{
                        productId: foundProduct.id,
                        price:foundProduct.price, 
                        quantity: newquantity , 
                        totalAmount: newtotal
                      }]
                  }
                console.log(userCart)
       

                res.overwrite(settingUpdate);

                return res.save().then((foundProduct) => {
                  console.log(`this is foundProduct: ${foundProduct}`)
                  console.log(`this is resCart: ${resCart}`)
                  globalThis.GetCartId = foundProduct.id
                  const resfoundProduct = foundProduct = true
                  return  resfoundProduct
                  
                  
                })
                .catch(err => {message:err.message})
              } else {
                //[section] if there are no existing in the find querry it saves the new add to cart by the user to the Cart collection
                return userCart.save().then((foundProduct) => {
                  console.log(`this is foundProduct: ${foundProduct}`)
                  console.log(`this is resCart: ${resCart}`)
                  globalThis.GetCartId = foundProduct.id
                  const resfoundProduct = foundProduct = true
                  return  resfoundProduct
                }).catch(err => {message:err.message})
              }
    }).catch(err => {message:err.message})
    //end
  
  }).catch(err => res.send(err))

  if(isCreatedCart !== true){
    return res.send({message: isCreatedCart})
  } 

  
   
 
    let isUserUpdated = await User.findByIdAndUpdate({_id: req.user.id}).then(userUpdate =>{
          console.log(`this getCart Id result: ${GetCartId}`)
          let customerCart = {
            cartId: GetCartId
          }

          console.log(userUpdate.cart.length)
          console.log(userUpdate)

          console.log(userUpdate.cart[0].includes(customerCart))

          for(let i; i < userUpdate.cart.length; i++){
            if(userUpdate.cart[i].cartId !== customerCart){
             return console.log('laman ng user cart' +userUpdate.cart[i].cartId)
              console.log('comparison' + customerCart)
             
            } 
          }
          if(userUpdate.cart.length !== 0){
              const overwriteCart = {
                firstName: userUpdate.firstName,
                lastName: userUpdate.lastName,
                email: userUpdate.email,
                password: userUpdate.password,
                isAdmin: userUpdate.isAdmin,
                mobileNo: userUpdate.mobileNo,
                cart: [{
                cartId: GetCartId,
                addedtoCartOn: new Date()
              }],
                orders: []
            }

            
    
        
              console.log('in update')
            
              for(let i; i < userUpdate.cart.length; i++){
                if(userUpdate.cart[i].cartId !== customerCart){
                  console.log('laman ng user cart' +userUpdate.cart[i].cartId)
                  console.log('comparison' + customerCart)
           
                } 
              }
          
           }else{
              //[section] if there are no existing in the find querry it saves the new add to cart by the user to the Cart collection
              console.log('in update sa else koya')
              userUpdate.cart.push(customerCart);
              return userUpdate.save().then(userUpdate => true).catch(err => err.message)
            }

                
      }).catch(err => {message:err.message})
      


    if(isUserUpdated !== true) {
      return res.send({ message: isUserUpdated})
     }   
     
     if(isCreatedCart && isUserUpdated) {
      return res.send({ message: " Successful added to cart."})
    }

}

//[SECTION]FUNCTIONALITY GET
  module.exports.getUserCart= (req, res) => {
    Cart.find({ userId: req.user.id }).then(result => res.send(result)).catch(err => res.send(err))
  }

  

  
//[SECTION]FUNCTIONALITY PUT





//[SECTION]FUNCTIONALITY DEL

module.exports.deleteCart = (id) => {
 
  return Cart.findByIdAndRemove(id).then((removedProduct, err) => {
    
     if (removedProduct === null){
        return res.send({message:'No Cart Was Removed, Cart does not exist'})
     } else {
        return res.send({message:'Succesfully removed to cart'})
     }
  });
}


