//[SECTION] Packages and Dependencies
const express = require("express"); 
const mongoose = require("mongoose");  
const cors = require("cors")

const dotenv = require("dotenv"); 
const userRoutes = require('./routes/users'); 
const productRoutes = require('./routes/products'); 
const orderRoutes = require('./routes/orders'); 
const cartRoutes = require('./routes/carts'); 

//[SECTION] Server Setup
const app = express(); 
dotenv.config(); 
app.use(express.json());
app.use(cors({
    origin: "*"
}));
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT; 

//[SECTION] Application Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/carts', cartRoutes);


//[SECTION] Database Connection
mongoose.connect(secret)
let connectStatus = mongoose.connection; 
connectStatus.on('open', () => console.log('Database is Connected'));

//[SECTION] Gateway Response
app.get('/', (req, res) => {
    res.send(`Welcome to FIRST NAME/s CUSTOM NAME APP`); 
 }); 
 app.listen(port, () => console.log(`Server is running on port ${port}`)); 
