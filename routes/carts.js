//[SECTION]Dependencies and modules
const exp = require("express"); 
const controller = require('../controllers/carts');
const auth = require("../loginAuths") 
const {verify, verifyAdmin} = auth;

//[SECTION]Routing Component
const route = exp.Router()

//[SECTION]POST (CREATE)
route.post("/userCart/:id", verify, controller.userCart)

//[SECTION]GET (RETRIEVE)

route.get("/getUserCart", verify, controller.getUserCart)
// route.get("/getAllOrder", verify, verifyAdmin, controller.getAllOrder)
// route.get("/getOrder", verify, verifyAdmin, controller.getOrder)

//[SECTION]PUT (UPDATE)

//[SECTION]DEL (DELETE)

//!! remove Cart 
route.delete('/:id', verify,(req, res) => {
  let id = req.params.id;
      controller.deleteCart(id).then(outcome => {
      res.send(outcome);
      });
});

//[SECTION] Export Route System

  module.exports = route; 