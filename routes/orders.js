//[SECTION]Dependencies and modules
const exp = require("express"); 
const controller = require('../controllers/orders');
const auth = require("../loginAuths") 
const {verify, verifyAdmin} = auth;

//[SECTION]Routing Component
const route = exp.Router()


//[SECTION]POST (CREATE)
route.post("/createOrder", verify, controller.createOrder)
// route.post("/createallOrder", verify, controller.createallOrder)


//[SECTION]GET (RETRIEVE)
route.get("/getUserOrder", verify, controller.getUserOrder)
route.get("/getAllOrder", verify, verifyAdmin, controller.getAllOrder)
route.get("/getOrder", verify, verifyAdmin, controller.getOrder)


//[SECTION]PUT (UPDATE)

//[SECTION]DEL (DELETE)

//[SECTION] Export Route System
  module.exports = route; 