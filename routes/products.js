//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/products');
const auth = require("../loginAuths")
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component

const route = exp.Router(); 

//[SECTION] [POST] Routes
  //! register products with admin only privelages

route.post('/register', verify, verifyAdmin, (req, res) => {
      let info = req.body;
      controller.createProduct(info).then(outcome => {
        res.send(outcome); 
      }); 
});


//[SECTION] [GET] Routes

route.get('/all',(req, res) => {
  controller.getAllProducts().then(outcome => {
    return res.send(outcome);
  });
});

route.get('/:id', (req, res) => {
  let data = req.params.id;
      controller.getProduct(data).then(outcome => {
          res.send(outcome);
      });
});

route.get('/', verify, (req, res) => {
  controller.getAllActiveProducts().then(outcome => {
      res.send(outcome);
  });
});

route.get('/archived', (req, res) => {
  controller.getAllArchivedProducts().then(outcome => {
      res.send(outcome);
  });
});



//[SECTION] [PUT] Routes
   //!  update product Admin only
   route.put('/:id',verify,verifyAdmin,(req, res) => {
    let id = req.params.id; 
    let details = req.body;
    let pName = details.name; 
    let pDesc = details.description;
    let pCost = details.price;       
    if (pName  !== '' && pDesc !== '' && pCost !== '') {
      controller.updateProduct(id, details).then(outcome => {
         return res.send(outcome); 
      });      
    } else {
        return res.send('Incorrect Input, Make sure details are complete');
    }
});

   //!  archive product Admin only
   route.put('/:id/archive',verify,verifyAdmin, (req, res) => {
        let productId = req.params.id
        controller.archive(productId).then(resultOfTheFunction => {  
           res.send(resultOfTheFunction)
        });
  });
    
   //!  re activating archived product Admin only
  route.put('/:id/activate',verify, verifyAdmin, (req, res) => {
        let productId = req.params.id;
        controller.activate(productId).then(result => {  
          res.send(result);
        });
 
  });


//[SECTION] [DEL] Routes
//!! delete product admin only
  route.delete('/:id', verify, (req, res) => {
      let id = req.params.id;
          controller.deleteProduct(id).then(outcome => {
          res.send(outcome);
          });
  });
//[SECTION] Export Route System

module.exports = route; 