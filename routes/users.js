//[SECTION] Dependencies and Modules
  const exp = require("express");
  const controller = require('../controllers/users');
  const auth = require("../loginAuths")
  const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 
 
//[SECTION] [POST] Routes
  route.post('/register', (req, res) => {
    let data = req.body; 
    controller.createUser(data).then(outcome => {
            res.send(outcome); 
    });
  });

  //! LOgin
  route.post("/login", controller.loginUser);

  //! GET USER DETAILS
  
  route.get("/getUserDetails", verify, controller.getUserDetails)


//[SECTION] [GET] Routes
  
route.get('/all', verify,verifyAdmin,(req, res) => {
    controller.getAllUsers().then(outcome => {
    res.send(outcome);
    });
  });

  route.get('/:id', (req, res) => {
    let data = req.params.id;
        controller.getUser(data).then(outcome => {
            res.send(outcome);
        });
  });

  route.get('/', (req, res) => {
    controller.getAllActiveUsers().then(outcome => {
        res.send(outcome);
    });
  });

 
//[SECTION] [PUT] Routes

  route.put('/setAdmin', verify, verifyAdmin,(req, res) => {
    let id = req.body.userid; 
    let details = req.body;
    let userEmail = details.email;
    let userisAdmin = details.isAdmin
    
    if (userisAdmin !== '') {
      controller.updateUsertoAdmin(id, details).then(outcome => {
        res.send(outcome);
      });

    } else if (id === "" || userisAdmin === "") {
      res.send ('no changes has been made to user')
    } 
  });


  route.put('/setToUser', verify, verifyAdmin,(req, res) => {
    let id = req.body.userid; 
    let details = req.body;
    let userEmail = details.email;
    let userisAdmin = details.isAdmin
    
    if (userisAdmin !== '') {
      controller.updateAdmintoUser(id, details).then(outcome => {
        res.send(outcome);
      });

    } else if (id === "" || userisAdmin === "") {
      res.send ('no changes has been made to user')
    } 
  });

//[SECTION] [DEL] Routes
  route.delete('/:id',verify, verifyAdmin,(req, res) => {
      let id = req.params.id;
          controller.deleteUser(id).then(outcome => {
          res.send(outcome);
          })
  });
//[SECTION] Export Route System

module.exports = route; 


